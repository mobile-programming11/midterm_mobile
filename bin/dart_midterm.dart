import 'package:dart_midterm/dart_midterm.dart' as dart_midterm;
import 'dart:io';
import 'dart:math';


String postfix = '123+*-8';
String evaluate = '123*+8';

void main(List<String> args) {
  String word = stdin.readLineSync()!;
  var tokens = exercise122(word);
  print("Tokens : $tokens");
  // var postfix = exercise122();
  // print("Your postfix : \n$postfix");
  exercise123infixtopostfix();
    // exercise124();
  exercise124evaluate();

}

List exercise122(String word) { //Exercise 122
  var tokens = word.split(''); 
  var tokens1 = new List<String>.filled(0, "m", growable: true);
  for (var token in tokens) {
    if (token != " ") {
      tokens1.add(token);
    }
  }
return tokens1;
}

void exercise123infixtopostfix() {
  var st = List.empty(growable: true); 
  var op = List.empty(growable: true); 
  var re = '';
  var x, y;
  for (var i = 0; i < postfix.length; i++) {// input postfix,length
    if (postfix[i] == '0' ||
        postfix[i] == '1' ||
        postfix[i] == '2' ||
        postfix[i] == '3' ||
        postfix[i] == '4' ||
        postfix[i] == '5' ||
        postfix[i] == '6' ||
        postfix[i] == '7' ||
        postfix[i] == '8' ||
        postfix[i] == '9') { 
      st.add(postfix[i]);
    } else if (postfix[i] == '+' ||
        postfix[i] == '-' ||
        postfix[i] == '*' ||
        postfix[i] == '/' ||
        postfix[i] == '%') {// + - * % 
      if (postfix[i] == '(' || postfix == ')') { x = 0;// ( , ) = 0
      } else if (postfix[i] == '+' || postfix[i] == '-') { x = 1;// + , - = 1 
      } else if (postfix[i] == '*' || postfix[i] == '/') { x = 2;// * , / = 2
      } else { x = 3;// % = 3 
      }


if (op.length > 0) {// input op.length = true
        if (op.last == '(' || op.last == ')') {
          y = 0; // ( , ) = 0
        } else if (op.last == '+' || op.last == '-') {
          y = 1;// + , - = 1 
        } else if (op.last == '*' || op.last == '/') {
          y = 2;// * , / = 2
        } else {
          y = 3;// % = 3 
        }
      }
      while (op.isNotEmpty && op.last != '(' && x <= y) { 
        String keep_op = op.last;
        op.removeLast();
        st.add(keep_op);
      }
      op.add(postfix[i]);
    }
    if (postfix[i] == '(') {
      op.add(postfix);
    } else if (postfix[i] == ')') {
      while (op.last != '(') {
        String keep_op = op.last;
        op.removeLast();
        st.add(keep_op);
      }
      op.remove('(');
    }
  }
  while (op.isNotEmpty) {
    String keep_op = op.last;
    op.removeLast();
    st.add(keep_op);
  }
  print('infix to postfix : $st');
}
void exercise124evaluate() {
  var st = List.empty(growable: true); 
  double num;
  for (int i = 0; i < evaluate.length; i++) {
    if (evaluate[i] == "0" ||
        evaluate[i] == "1" ||
        evaluate[i] == "2" ||
        evaluate[i] == "3" ||
        evaluate[i] == "4" ||
        evaluate[i] == "5" ||
        evaluate[i] == "6" ||
        evaluate[i] == "7" ||
        evaluate[i] == "8" ||
        evaluate[i] == "9") {
      switch (evaluate[i]) {
        case "0": { num = 0; st.add(num);
          }
          break;
        case "1": { num = 1; st.add(num);
          }
          break;
        case "2": { num = 2; st.add(num);
          }
          break;
        case "3": { num = 3; st.add(num);
          }
          break;
        case "4": { num = 4; st.add(num);
          }
          break;
        case "5": { num = 5; st.add(num);
          }
          break;
        case "6": { num = 6; st.add(num);
          }
          break;
        case "7": { num = 7; st.add(num);
          }
          break;
        case "8": { num = 8; st.add(num);
          }
          break;
        case "9": { num = 9; st.add(num);
          }
          break;
      }

    } else {
      double right;
      double left;
      right = st.last;
      st.removeLast();
      left = st.last;
      st.removeLast();
      double a = 0;
      if (evaluate[i] == "+") {
        a = (left + right);
      }
      if (evaluate[i] == "-") {
        a = (left - right);
      }
      if (evaluate[i] == "*") {
        a = (left * right);
      }
      if (evaluate[i] == "/") {
        a = (left / right);
      }
      if (evaluate[i] == "%") {
        a = (left % right);
      }
      st.add(a);
    }
  }
  print('evaluate postfix : $st');
}